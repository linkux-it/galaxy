vim.loader.enable()

-- Galaxy variables
local galaxy_version = "3.0"

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

----------------------------------------------------------------------
-- Galaxy global variables
----------------------------------------------------------------------

vim.g.galaxy_version = galaxy_version

----------------------------------------------------------------------
-- Galaxy global setup
----------------------------------------------------------------------

vim.o.smoothscroll = true
vim.o.mousemoveevent = true

----------------------------------------------------------------------
-- Packer Manager setup
----------------------------------------------------------------------

if not vim.uv.fs_stat(lazypath) then
  print("Installing engine...")

  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })

  print("Base engine installed")
end

vim.opt.rtp:append(lazypath)

require("galaxy")
