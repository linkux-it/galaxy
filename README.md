# Galaxy

## Update package list with latest used

Is a neovim setup like spacemacs and based on spacevim

![Version 2.0](https://img.shields.io/badge/version-2.0-green.svg?style=flat-square)
[![MIT License](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](LICENSE)

# Fonts instalations

To install fonts do this https://github.com/ryanoasis/nerd-fonts#option-2-ad-hoc-curl-download  
To enable fonts do this for OSX https://github.com/ryanoasis/nerd-fonts/issues/13#issuecomment-190252936  

### Table of Contents
- [Introduction](#introduction)
- [Install](#install)
  - [Linux/Mac](#linuxmac)
  - Windows support
- [File Structure](#file-structure)

#### Introduction

[Galaxy](https://gitlab.com/linkux-it/galaxy) is an opinioned configuration for developers with
minimal setup, focused in plugins that will provide features like VSCode or any other IDE/TextEditor
also focused in LSP and TreeSitter neovim implementation.

It got inspired by [spacemacs] and [SpaceVim](https://spacevim.org).

If you encounter any bugs or have feature requests, just open an issue report on Github.

For learning about Vim in general, read [vim-galore](https://github.com/mhinz/vim-galore).

#### Install

##### Linux/Mac

```sh
curl -sLf https://gitlab.com/linkux-it/galaxy/raw/master/install.sh | bash
```

Installation of neovim
> [neovim installation](https://github.com/neovim/neovim/wiki/Installing-Neovim)


#### File Structure
The file Structure is defined to allow user change some(maybe all) default values setups.

- [lua](./lua)/ - Lua configuration for some modules
- [init.toml](./init.toml) - Toml file for basic configs

#### Base plugins

##### Packer
This is the packer

##### PopUp

#### TreeSitter

Configure base treesitter but allow configure more modules and setup.

See treesitter plugin used: [nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter)

#### LSP

See plugins used to use as many LSP features as we can: 

- [nvim-lspconfig](https://github.com/neovim/nvim-lspconfig)
- [nvim-cmp](https://github.com/hrsh7th/nvim-cmp)
- [nvim-lspinstall](https://github.com/kabouzeid/nvim-lspinstall)
- [lspkind-nvim](https://github.com/onsails/lspkind-nvim)
- [symbols-outline](https://github.com/simrat39/symbols-outline.nvim)
- [nvim-lsputils](https://github.com/RishabhRD/nvim-lsputils)

To enable servers you have to define and return a table with servers into `.Galaxy.d/lua/lspservers.lua`


# Old Documentation


- [Basics](#basic)
- [Neovim centric - Dark powered mode](#neovim-centric---dark-powered-mode-of-spacevim)
- [Modular configuration](#modular-configuration)
- [Mappings](#mappings)
- [Worlds](#worlds)
  - [Layout](#layout)
  - [Vim](#vim)
  - [Javascript](#javascript)
  - [Flow](#flow)
  - [Git](#git)
  - [Python](#python)
  - [Django](#django)
  - [Web](#web)
  - [React](#react)
  - [Typescript](#typescript)
  - [Nginx](#nginx)
  - [GraphQL](#graphql)
  - [Tests](#tests)
  - [Meetings](#meetings)
  - [Shell](#shell)
  - [Rust](#rust)
  - [Databases](#databases)
  - [PHP](#php)
- [Credits & Thanks](#credits--thanks)


#### Basics

<leader> is set to <space> all commands will fall into that same as [spacemacs].

#### Modular configuration
All plugins config are under g:galaxy_user_dir/config directory will overide default ones

Config are under g:galaxy_user_dir/init.vim will overide default ones

#### Mappings
Tries to emulate all about spacemacs read the [docs]

#### Worlds

##### Layout

Name           | Description
-------------- | ----------------------
[FastFold] | Speed up Vim by updating folds only when called-for.
[NrrwRgn] | A Narrow Region Plugin for vim (like Emacs Narrow Region)
[PreserveNoEOL] | Preserve missing EOL at the end of text files.
[ZoomWin] | Zoom in/out of windows (toggle between one window and multi-window)
[abolish] | Easily search for, substitute, and abbreviate multiple variants of a word
[airline-themes] | A collection of themes for vim-airline
[airline] | Lean & mean status/tabline for vim that's light as air
[ale] | ALE (Asynchronous Lint Engine) is a plugin for providing linting in NeoVim and Vim 8 while you edit your text files.
[asterisk] | -Improved
[bufkill] | In Vim, trying to unload, delete or wipe a buffer without closing the window or split
[case-convert] | Makes it easy to switch whole files between snake case, hyphenation and camel case.
[comment] | An extensible & universal comment vim-plugin that also handles embedded filetypes
[context_filetype] | Context filetype library for Vim script
[ctrlsf] | An ack/ag/pt/rg powered code search and view tool, takes advantage of Vim 8's power to support asynchronous searching, and lets you edit file in-place with Edit Mode.
[packer] | use-package inspired plugin/package management for Neovim.
[nvim-tree] | A File Explorer For Neovim Written In Lua
[denite] | Dark powered asynchronous unite all interfaces for Neovim/Vim8
[nvim-lsp] | Common configurations for Neovim Language Servers
[devicons] | dds file type glyphs/icons to popular Vim plugins.
[endwise] | Wisely add "end" in ruby, endfunction/endif/more in vim script, etc
[expand-region] | Vim plugin that allows you to visually select increasingly larger regions of text using the same key combination.
[far] | Find And Replace Vim plugin
[fetch] | Make Vim handle line and column numbers in file names with a minimum of fuss.
[fruzzy] | Freaky fast fuzzy finder (Denite matcher) for neovim
[goyo] | Distraction-free writing in Vim
[grepper] | Use your favorite grep tool to start an asynchronous search.
[gutentags] | Gutentags is a plugin that takes care of the much needed management of tags files in Vim
[hardtime] | Hardtime helps you break that annoying habit vimmers
[indentLine] | A vim plugin to display the indention levels with thin vertical lines
[lexima] | Auto close parentheses and repeat by dot dot dot...
[limelight] | All the world's indeed a stage and we are merely players
[localvimrc] | This plugin searches for local vimrc files in the file system tree of the currently opened file.
[move] | Plugin to move lines and selections up and down
[mundo] | Vim undo tree visualizer
[neco-syntax] | Syntax source for neocomplete/deoplete
[neoinclude] | Include completion framework for neocomplete/deoplete
[neomru] | MRU plugin includes unite.vim MRU sources
[obsession] | Continuously updated session files
[projectionist] | Projectionist provides granular project configuration using "projections".
[relativity] | Toggle relative line numbers with ease.
[repeat] | Enable repeating supported plugin maps with "."
[searchtasks] | Plugin to search the labels often used as TODO, FIXME and XXX.
[sort-motion] | This plugin provides the ability to sort in Vim using text objects and motions.
[startify] | Vim Startify
[sandwich] | is a set of operator and textobject plugins to add/delete/replace surroundings of a sandwiched textobject, like (foo), "bar".
[taboo] | Taboo aims to ease the way you set the vim tabline. In addition, Taboo provides fews useful utilities for renaming tabs.
[tabular] | Vim script for text filtering and alignment
[targets] | Targets.vim is a Vim plugin that adds various text objects
[togglelist] | Functions to toggle the [Location List] and the [Quickfix List] windows.
[unimpaired] | Pairs of handy bracket mappings
[vista] | View and search LSP symbols, tags in Vim/NeoVim.
[completion] | completion-nvim is a auto completion framework aims to provide a better completion experience with neovim's built-in LSP. Other LSP sources is not supported.
[diagnostic] | 	A wrapper for neovim built in LSP diagnosis config 
[status] | **set description here**

##### Vim

Name           | Description
-------------- | ----------------------
[scriptease] | Vim plugin for making Vim plugins
[vimlint] | Lint for vim script
[vimlparser] | VimL parser

##### Javascript

Name           | Description
-------------- | ----------------------

##### Flow

Name           | Description
-------------- | ----------------------
[nvim-lsp] | configure flow

##### Git
Name           | Description
-------------- | ----------------------
[agit] | A powerful Git log viewer
[gina] | Alpha: Git manipulation plugin for Neovim/Vim8 (new version of vim-gita)
[signify] | Show a diff using Vim its sign column.

##### Python

Name           | Description
-------------- | ----------------------
[virtualenv] | Vim plugin for working with python virtualenvs

##### Django

Name           | Description
-------------- | ----------------------
[django-plus] | Improvements to the handling of Django related files in Vim, some of which are based on Steve Losh's htmldjango scripts.

##### Lua

Name           | Description
-------------- | ----------------------
[nvim-lsp] | configure lua

##### Web

Name           | Description
-------------- | ----------------------
[MatchTagAlways] | A Vim plugin that always highlights the enclosing html/xml tags
[closetag] | Auto close (X)HTML tags
[nvim-colorizer.lua] | A high-performance color highlighter for Neovim which has no external dependencies! Written in performant Luajit.
[emmet] | emmet for vim
[html5] | HTML5 omnicomplete and syntax
[less] | vim syntax for LESS (dynamic CSS)
[markdown-preview] | preview markdown on your modern browser with sync scroll and flexible configuration
[markdown] | Markdown Vim Mode
[mustache] | Vim mode for mustache and handlebars (Deprecated)
[postcss] | PostCSS Syntax for Vim
[pug] | Vim Pug (formerly Jade) template engine syntax highlighting and indention
[scss-syntax] | Vim syntax file for scss (Sassy CSS)
[stylus] | Syntax Highlighting for Stylus

##### React

Name           | Description
-------------- | ----------------------
[treesitter] | configure javascript

##### Typescript

Name           | Description
-------------- | ----------------------
[nvim-lsp] | configure typescript

##### Nginx

Name           | Description
-------------- | ----------------------
[nginx] | Improved nginx vim plugin (incl. syntax highlighting)

##### GraphQL

Name           | Description
-------------- | ----------------------
[graphql] | A Vim plugin that provides GraphQL file detection and syntax highlighting.

##### Tests

Name           | Description
-------------- | ----------------------
[test] | A Vim wrapper for running tests on different granularities.

##### Meetings

Name           | Description
-------------- | ----------------------
[presenting] | is a vim plugin that turns your markup into presentable slides (in vim).

##### Shell

Name           | Description
-------------- | ----------------------
[deol] | Dark powered shell interface for NeoVim.

##### Rust

Name           | Description
-------------- | ----------------------
[rust]         | Vim configuration for Rust

##### Databases

Name           | Description
-------------- | ----------------------
[dadbod] | Dadbod is a Vim plugin for interacting with databases.

##### PHP

Name           | Description
-------------- | ----------------------
[PHP-Indenting-for-VIm] | This is the official PHP indentation plug-in for VIm
[php] | An up-to-date Vim syntax for PHP (7.x supported)
[phpfolding] | This script can fold PHP functions and/or classes, properties 


#### Credits & Thanks
- Contributors
- [SpaceVim](https://github.com/SpaceVim/SpaceVim)

[FastFold]: https://github.com/Konfekt/FastFold
[MatchTagAlways]: https://github.com/valloric/MatchTagAlways
[NrrwRgn]: https://github.com/chrisbra/NrrwRgn
[PHP-Indenting-for-VIm]: https://github.com/2072/PHP-Indenting-for-VIm
[PreserveNoEOL]: https://github.com/vim-scripts/PreserveNoEOL
[ZoomWin]: https://github.com/vim-scripts/ZoomWin
[abolish]: https://github.com/tpope/vim-abolish
[agit]: https://github.com/cohama/agit.vim
[airline-themes]: https://github.com/vim-airline/vim-airline-themes
[airline]: https://github.com/vim-airline/vim-airline
[ale]: https://github.com/dense-analysis/ale
[asterisk]: https://github.com/haya14busa/vim-asterisk
[bufkill]: https://github.com/qpkorr/vim-bufkill
[case-convert]: https://github.com/chiedo/vim-case-convert
[closetag]: https://github.com/alvan/vim-closetag
[comment]: https://github.com/tomtom/tcomment_vim
[context_filetype]: https://github.com/Shougo/context_filetype.vim
[ctrlsf]: https://github.com/dyng/ctrlsf.vim
[dadbod]: https://github.com/tpope/vim-dadbod
[packer]: https://github.com/wbthomason/packer.nvim
[denite]: https://github.com/Shougo/denite.nvim
[deol]: https://github.com/Shougo/deol.nvim
[deoppet.nvim]: https://github.com/Shougo/deoppet.nvim
[devicons]: https://github.com/ryanoasis/vim-devicons
[django-plus]: https://github.com/tweekmonster/django-plus.vim
[emmet]: https://github.com/mattn/emmet-vim
[endwise]: https://github.com/tpope/vim-endwise
[expand-region]: https://github.com/terryma/vim-expand-region
[far]: https://github.com/brooth/far.vim
[fetch]: https://github.com/kopischke/vim-fetch
[fruzzy]: https://github.com/raghur/fruzzy
[gina]: https://github.com/lambdalisue/gina.vim
[goyo]: https://github.com/junegunn/goyo.vim
[graphql]: https://github.com/jparise/vim-graphql
[grepper]: https://github.com/mhinz/vim-grepper
[gutentags]: https://github.com/ludovicchabant/vim-gutentags
[hardtime]: https://github.com/takac/vim-hardtime
[html5]: https://github.com/othree/html5.vim
[indentLine]: https://github.com/Yggdroot/indentLine
[less]: https://github.com/groenewege/vim-less
[lexima]: https://github.com/cohama/lexima.vim
[limelight]: https://github.com/junegunn/limelight.vim
[localvimrc]: https://github.com/embear/vim-localvimrc
[markdown-preview]: https://github.com/iamcco/markdown-preview.nvim
[markdown]: https://github.com/plasticboy/vim-markdown
[move]: https://github.com/matze/vim-move
[mundo]: https://github.com/simnalamburt/vim-mundo
[mustache]: https://github.com/juvenn/mustache.vim
[neco-syntax]: https://github.com/Shougo/neco-syntax
[neoinclude]: https://github.com/Shougo/neoinclude.vim
[neomru]: https://github.com/Shougo/neomru.vim
[nginx]: https://github.com/chr4/nginx.vim
[nvim-tree]: https://github.com/kyazdani42/nvim-tree.lua
[obsession]: https://github.com/tpope/vim-obsession
[php]: https://github.com/StanAngeloff/php.vim
[phpfolding]: https://github.com/rayburgemeestre/phpfolding.vim
[postcss]: https://github.com/stephenway/postcss.vim
[presenting]: https://github.com/sotte/presenting.vim
[projectionist]: https://github.com/tpope/vim-projectionist
[pug]: https://github.com/digitaltoad/vim-pug
[relativity]: https://github.com/kennykaye/vim-relativity
[repeat]: https://github.com/tpope/vim-repeat
[rust]: https://github.com/rust-lang/rust.vim
[scriptease]: https://github.com/tpope/vim-scriptease
[scss-syntax]: https://github.com/cakebaker/scss-syntax.vim
[searchtasks]: https://github.com/gilsondev/searchtasks.vim
[signify]: https://github.com/mhinz/vim-signify
[sort-motion]: https://github.com/christoomey/vim-sort-motion
[spacemacs]: https://github.com/syl20bnr/spacemacs
[startify]: https://github.com/mhinz/vim-startify
[stylus]: https://github.com/wavded/vim-stylus
[sandwich]: https://github.com/machakann/vim-sandwich
[taboo]: https://github.com/gcmt/taboo.vim
[tabular]: https://github.com/godlygeek/tabular
[targets]: https://github.com/wellle/targets.vim
[test]: https://github.com/janko-m/vim-test
[togglelist]: https://github.com/milkypostman/vim-togglelist
[unimpaired]: https://github.com/tpope/vim-unimpaired
[nvim-colorizer.lua]: https://github.com/norcalli/nvim-colorizer.lua
[vimlint]: https://github.com/syngan/vim-vimlint
[vimlparser]: https://github.com/ynkdir/vim-vimlparser
[virtualenv]: https://github.com/jmcantrell/vim-virtualenv
[vista]: https://github.com/liuchengxu/vista.vim
[completion]: https://github.com/nvim-lua/completion-nvim/
[diagnostic]: https://github.com/nvim-lua/diagnostic-nvim/
[status]: https://github.com/nvim-lua/lsp-status/
[nvim-lsp]: https://github.com/neovim/nvim-lsp
