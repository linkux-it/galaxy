#!/usr/bin/env bash

# A guarding function to avoid executing an incompletely downloaded script
guard () {

# Reset
Color_off='\033[0m'       # Text Reset

# Regular Colors
Red='\033[0;31m'          # Red
Blue='\033[0;34m'         # Blue

need_cmd () {
    if ! hash "$1" &>/dev/null; then
        echo -e "${Red}need '$1' (command not found)${Color_off}"
        exit 1
    fi
}

fetch_repo () {
    if [[ -d "$HOME/.Galaxy" ]]; then
        git --git-dir "$HOME/.Galaxy/.git" pull
        echo -e "${Blue}Successfully update Galaxy${Color_off}"
    else
        git clone https://gitlab.com/linkux-it/galaxy.git "$HOME/.Galaxy"
        echo -e "${Blue}Successfully clone Galaxy${Color_off}"
    fi
}

install_neovim () {
    if [[ -d "$HOME/.config/nvim" ]]; then
        if [[ "$(readlink $HOME/.config/nvim)" =~ \.Galaxy$ ]]; then
            echo -e "${Blue}Installed Galaxy for neovim${Color_off}"
        else
            mv "$HOME/.config/nvim" "$HOME/.config/nvim_back"
            echo -e "${Blue}BackUp $HOME/.config/nvim${Color_off}"
            ln -s "$HOME/.Galaxy" "$HOME/.config/nvim"
            echo -e "${Blue}Installed Galaxy for neovim${Color_off}"
        fi
    else
        ln -s "$HOME/.Galaxy" "$HOME/.config/nvim"
        echo -e "${Blue}Installed Galaxy for neovim${Color_off}"
    fi
}

uninstall_neovim () {
    if [[ -d "$HOME/.config/nvim" ]]; then
        if [[ "$(readlink $HOME/.config/nvim)" =~ \.Galaxy$ ]]; then
            rm "$HOME/.config/nvim"
            echo -e "${Blue}Uninstall Galaxy for neovim${Color_off}"
            if [[ -d "$HOME/.config/nvim_back" ]]; then
                mv "$HOME/.config/nvim_back" "$HOME/.config/nvim"
                echo -e "${Blue}Recover $HOME/.config/nvim${Color_off}"
            fi
        fi
    fi
}

usage () {
    echo "Galaxy install script : V 1.0"
    echo "    Install Galaxy for neovim"
    echo "        curl -sLf https://gitlab.com/linkux-it/galaxy/raw/master/install.sh | bash"
    echo "        or"
    echo "    Uninstall Galaxy"
    echo "        curl -sLf https://gitlab.com/linkux-it/galaxy/raw/master/install.sh | bash -s -- uninstall"
}


if [ $# -gt 0 ]
then
    case $1 in
        uninstall)
            uninstall_neovim
            exit 0
            ;;
        install)
            need_cmd 'git'
            fetch_repo
            install_neovim
            exit 0
            ;;
        -h)
            usage
            exit 0
    esac
fi
# if no argv, installer will install Galaxy
need_cmd 'git'
fetch_repo
install_neovim

# end of guard
}

# download finished fine
guard $@
