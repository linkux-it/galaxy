require("user.configs.before")

require("lazy").setup({
  checker = {
    enabled = true,
  },
  spec = {
    { import = "galaxy.plugins" },
    { import = "user.plugins" },
  },
})

vim.schedule(function()
  require("user.configs.after")
end)
