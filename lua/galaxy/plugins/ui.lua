vim.o.laststatus = 3

vim.opt.fillchars = {
  stl = "─",
}

return {
  {
    "sschleemilch/slimline.nvim",
    event = "VeryLazy",
    opts = {
      style = "fg",
      bold = true,
      verbose_mode = true,
      mode_follow_style = false,
      workspace_diagnostics = true,
      components = { -- Choose components and their location
        left = {
          "mode",
          "path",
          "git",
          "diagnostics",
        },
        center = {
          "recording",
        },
        right = {
          "filetype_lsp",
          "progress",
        },
      },
      icons = {
        diagnostics = {
          ERROR = "✗ ",
          WARN = " ",
          HINT = " ",
          INFO = "🛈",
        },
      },
      spaces = {
        components = "─",
        left = "─",
        right = "─",
      },
    },
  },
  {
    "hat0uma/csvview.nvim",
    -- ft = { 'csv' },
    opts = {},
  },
  -- {
  --   "nvim-lualine/lualine.nvim",
  --   event = "VeryLazy",
  --   init = function()
  --     vim.g.lualine_laststatus = vim.o.laststatus
  --     if vim.fn.argc(-1) > 0 then
  --       -- set an empty statusline till lualine loads
  --       vim.o.statusline = " "
  --     else
  --       -- hide the statusline on the starter page
  --       vim.o.laststatus = 0
  --     end
  --   end,
  --   opts = {
  --     options = {
  --       globalstatus = true,
  --     },
  --     extensions = { "quickfix", "nvim-tree", "symbols-outline", "toggleterm" },
  --     disabled_filetypes = { statusline = { "dashboard", "alpha", "starter" } },
  --     sections = {
  --       lualine_x = {
  --         {
  --           function()
  --             return require("noice").api.status.command.get()
  --           end,
  --           cond = function()
  --             return package.loaded["noice"] and require("noice").api.status.command.has()
  --           end,
  --           -- color = LazyVim.ui.fg("Statement"),
  --         },
  --         {
  --           function()
  --             return require("noice").api.status.mode.get()
  --           end,
  --           cond = function()
  --             return package.loaded["noice"] and require("noice").api.status.mode.has()
  --           end,
  --           -- color = LazyVim.ui.fg("Constant"),
  --         },
  --         {
  --           function()
  --             return "  " .. require("dap").status()
  --           end,
  --           cond = function()
  --             return package.loaded["dap"] and require("dap").status() ~= ""
  --           end,
  --           -- color = LazyVim.ui.fg("Debug"),
  --         },
  --         "encoding",
  --         "fileformat",
  --         "filetype",
  --       },
  --       lualine_c = {
  --         "filename",
  --         {
  --           require("lazy.status").updates,
  --           cond = require("lazy.status").has_updates,
  --           color = { fg = "#ff9e64" },
  --         },
  --       },
  --     },
  --   },
  -- },
  {
    "akinsho/bufferline.nvim",
    event = "VeryLazy",
    dependencies = "nvim-tree/nvim-web-devicons",
    opts = {
      options = {
        diagnostics = "nvim_lsp",
        always_show_bufferline = false,
        separator_style = "slope",
        modified_icon = "",
        offsets = {
          filetype = "neo-tree",
          text = "File Explorer",
          highlight = "Directory",
          text_align = "left",
          separator = true,
        },
        numbers = function(opts)
          return string.format("%s", opts.ordinal)
          -- return string.format("%s%s", opts.id, opts.raise(opts.ordinal))
        end,
      },
    },
    keys = {
      {
        "<leader>1",
        function()
          require("bufferline").go_to(1, true)
        end,
        desc = "Move to buffer 1",
      },
      {
        "<leader>2",
        function()
          require("bufferline").go_to(2, true)
        end,
        desc = "Move to buffer 2",
      },
      {
        "<leader>3",
        function()
          require("bufferline").go_to(3, true)
        end,
        desc = "Move to buffer 3",
      },
      {
        "<leader>4",
        function()
          require("bufferline").go_to(4, true)
        end,
        desc = "Move to buffer 4",
      },
      {
        "<leader>5",
        function()
          require("bufferline").go_to(5, true)
        end,
        desc = "Move to buffer 5",
      },
      {
        "<leader>6",
        function()
          require("bufferline").go_to(6, true)
        end,
        desc = "Move to buffer 6",
      },
      {
        "<leader>7",
        function()
          require("bufferline").go_to(7, true)
        end,
        desc = "Move to buffer 7",
      },
      {
        "<leader>8",
        function()
          require("bufferline").go_to(8, true)
        end,
        desc = "Move to buffer 8",
      },
      {
        "<leader>9",
        function()
          require("bufferline").go_to(9, true)
        end,
        desc = "Move to buffer 9",
      },
      {
        "<leader>-1",
        function()
          require("bufferline").go_to(-1, true)
        end,
        desc = "Move to buffer -1",
      },
    },
  },
  {
    "folke/noice.nvim",
    event = "VeryLazy",
    opts = {
      lsp = {
        override = {
          ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
          ["vim.lsp.util.stylize_markdown"] = true,
        },
        -- use blink.cmp signature instead of noice one
        signature = {
          enabled = false,
        },
      },
      presets = {
        command_palette = true, -- position the cmdline and popupmenu together
        lsp_doc_border = true, -- add a border to hover docs and signature help
        long_message_to_split = true,
        bottom_search = false,
      },
      routes = {
        {
          filter = {
            event = "msg_show",
            any = {
              { find = "%d+L, %d+B" },
              { find = "; after #%d+" },
              { find = "; before #%d+" },
            },
          },
          view = "mini",
        },
      },
    },
  },
  {
    "rcarriga/nvim-notify",
    keys = {
      {
        "<leader>un",
        function()
          require("notify").dismiss({ silent = true, pending = true })
        end,
        desc = "Dismiss All Notifications",
      },
    },
    opts = {
      stages = "static",
      timeout = 3000,
      max_height = function()
        return math.floor(vim.o.lines * 0.75)
      end,
      max_width = function()
        return math.floor(vim.o.columns * 0.75)
      end,
      on_open = function(win)
        vim.api.nvim_win_set_config(win, { zindex = 100 })
      end,
    },
  },
  {
    "lukas-reineke/indent-blankline.nvim",
    event = "VeryLazy",
    config = function(_, opts)
      local hooks = require("ibl.hooks")
      -- create the highlight groups in the highlight setup hook, so they are reset
      -- every time the colorscheme changes
      hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
        vim.api.nvim_set_hl(0, "RainbowRed", { fg = "#E06C75" })
        vim.api.nvim_set_hl(0, "RainbowYellow", { fg = "#E5C07B" })
        vim.api.nvim_set_hl(0, "RainbowBlue", { fg = "#61AFEF" })
        vim.api.nvim_set_hl(0, "RainbowOrange", { fg = "#D19A66" })
        vim.api.nvim_set_hl(0, "RainbowGreen", { fg = "#98C379" })
        vim.api.nvim_set_hl(0, "RainbowViolet", { fg = "#C678DD" })
        vim.api.nvim_set_hl(0, "RainbowCyan", { fg = "#56B6C2" })
      end)
      require("ibl").setup(opts)
    end,
    opts = {
      indent = {
        char = { "|", "¦", "┆", "┊" },
        tab_char = { "|", "¦", "┆", "┊" },
        highlight = {
          "RainbowRed",
          "RainbowYellow",
          "RainbowBlue",
          "RainbowOrange",
          "RainbowGreen",
          "RainbowViolet",
          "RainbowCyan",
        },
      },
      scope = { show_start = false, show_end = false },
      exclude = {
        filetypes = {
          -- "neo-tree",
          -- "Trouble",
          -- "trouble",
          "lazy",
          "mason",
          "notify",
          "toggleterm",
          "lspinfo",
          "checkhealth",
          "help",
          "man",
          "",
        },
      },
    },
  },
  {
    "nkakouros-original/numbers.nvim",
    event = "VeryLazy",
    opts = {
      excluded_filetypes = {
        TelescopePrompt = true,
        ["neo-tree"] = true,
      },
    },
    init = function()
      vim.o.number = true
    end,
  },
  {
    "luukvbaal/statuscol.nvim",
    event = "VeryLazy",
    config = function()
      local builtin = require("statuscol.builtin")
      require("statuscol").setup({
        segments = {
          { text = { "%s" }, click = "v:lua.ScSa" },
          { text = { builtin.lnumfunc }, click = "v:lua.ScLa" },
          {
            text = { " ", builtin.foldfunc, " " },
            condition = { builtin.not_empty, true, builtin.not_empty },
            click = "v:lua.ScFa",
          },
        },
      })
    end,
  },
  {
    "akinsho/nvim-toggleterm.lua",
    event = "VeryLazy",
    opts = {
      open_mapping = [[<m-\>]],
      shade_filetypes = {},
      shade_terminals = true,
      size = 25,
      float_opts = {
        border = "curved",
        width = 170,
        height = 45,
        -- winblend = 10,
      },
      winbar = {
        enabled = true,
      },
    },
  },
  {
    "nvim-telescope/telescope.nvim",
    cmd = "Telescope",
    config = true,
    opts = {
      defaults = {
        prompt_prefix = " ",
        selection_caret = " ",
      },
    },
    keys = {
      -- find
      {
        "<leader>fb",
        function()
          require("telescope.builtin").buffers({ sort_mru = true })
        end,
        desc = "Switch Buffer",
      },
      {
        "<leader>ff",
        function()
          require("telescope.builtin").find_files()
        end,
        desc = "Find Files",
      },
      -- search
      -- { "<leader>/", LazyVim.telescope("live_grep"), desc = "Grep (Root Dir)" },
      -- { "<leader>:", "<cmd>Telescope command_history<cr>", desc = "Command History" },
      -- { "<leader>fc", LazyVim.telescope.config_files(), desc = "Find Config File" },
      -- { "<leader>fg", "<cmd>Telescope git_files<cr>", desc = "Find Files (git-files)" },
      -- { "<leader>fr", "<cmd>Telescope oldfiles<cr>", desc = "Recent" },
      -- { "<leader>fR", LazyVim.telescope("oldfiles", { cwd = vim.uv.cwd() }), desc = "Recent (cwd)" },
      -- -- git
      -- { "<leader>gc", "<cmd>Telescope git_commits<CR>", desc = "Commits" },
      -- { "<leader>gs", "<cmd>Telescope git_status<CR>", desc = "Status" },
      -- { '<leader>s"', "<cmd>Telescope registers<cr>", desc = "Registers" },
      -- { "<leader>sa", "<cmd>Telescope autocommands<cr>", desc = "Auto Commands" },
      -- { "<leader>sb", "<cmd>Telescope current_buffer_fuzzy_find<cr>", desc = "Buffer" },
      -- { "<leader>sc", "<cmd>Telescope command_history<cr>", desc = "Command History" },
      -- { "<leader>sC", "<cmd>Telescope commands<cr>", desc = "Commands" },
      -- { "<leader>sd", "<cmd>Telescope diagnostics bufnr=0<cr>", desc = "Document Diagnostics" },
      -- { "<leader>sD", "<cmd>Telescope diagnostics<cr>", desc = "Workspace Diagnostics" },
      -- { "<leader>sg", LazyVim.telescope("live_grep"), desc = "Grep (Root Dir)" },
      -- { "<leader>sG", LazyVim.telescope("live_grep", { cwd = false }), desc = "Grep (cwd)" },
      -- { "<leader>sh", "<cmd>Telescope help_tags<cr>", desc = "Help Pages" },
      -- { "<leader>sH", "<cmd>Telescope highlights<cr>", desc = "Search Highlight Groups" },
      -- { "<leader>sk", "<cmd>Telescope keymaps<cr>", desc = "Key Maps" },
      -- { "<leader>sM", "<cmd>Telescope man_pages<cr>", desc = "Man Pages" },
      -- { "<leader>sm", "<cmd>Telescope marks<cr>", desc = "Jump to Mark" },
      -- { "<leader>so", "<cmd>Telescope vim_options<cr>", desc = "Options" },
      -- { "<leader>sR", "<cmd>Telescope resume<cr>", desc = "Resume" },
      -- { "<leader>sw", LazyVim.telescope("grep_string", { word_match = "-w" }), desc = "Word (Root Dir)" },
      -- { "<leader>sW", LazyVim.telescope("grep_string", { cwd = false, word_match = "-w" }), desc = "Word (cwd)" },
      -- { "<leader>sw", LazyVim.telescope("grep_string"), mode = "v", desc = "Selection (Root Dir)" },
      -- { "<leader>sW", LazyVim.telescope("grep_string", { cwd = false }), mode = "v", desc = "Selection (cwd)" },
      -- { "<leader>uC", LazyVim.telescope("colorscheme", { enable_preview = true }), desc = "Colorscheme with Preview" },
      -- {
      --   "<leader>ss",
      --   function()
      --     require("telescope.builtin").lsp_document_symbols({
      --       symbols = require("lazyvim.config").get_kind_filter(),
      --     })
      --   end,
      --   desc = "Goto Symbol",
      -- },
      -- {
      --   "<leader>sS",
      --   function()
      --     require("telescope.builtin").lsp_dynamic_workspace_symbols({
      --       symbols = require("lazyvim.config").get_kind_filter(),
      --     })
      --   end,
      --   desc = "Goto Symbol (Workspace)",
      -- },
    },
  },
  {
    "folke/which-key.nvim",
    event = "VeryLazy",
    opts = {
      preset = "modern",
      plugins = { spelling = true },
      spec = {
        mode = { "n", "v" },
        { "g", group = "goto" },
        { "ms", group = "surround" },
        { "z", group = "fold" },
        { ",", group = "next" },
        { "[", group = "prev" },
        { "<leader><tab>", group = "tabs" },
        { "<leader>b", group = "buffer" },
        { "<leader>c", group = "code" },
        { "<leader>f", group = "file/find" },
        { "<leader>g", group = "git" },
        { "<leader>gh", group = "hunks" },
        { "<leader>q", group = "quit/session" },
        { "<leader>s", group = "search" },
        { "<leader>u", group = "ui" },
        { "<leader>w", group = "windows" },
        { "<leader>x", group = "diagnostics/quickfix" },
      },
    },
  },
  {
    "kevinhwang91/nvim-ufo",
    dependencies = "kevinhwang91/promise-async",
    config = true,
    event = "VeryLazy",
    keys = {
      {
        "zR",
        function()
          require("ufo").openAllFolds()
        end,
        mode = { "n" },
      },
      {
        "zM",
        function()
          require("ufo").closeAllFolds()
        end,
        mode = { "n" },
      },
    },
    init = function()
      vim.o.foldcolumn = "1" -- '0' is not bad
      vim.o.foldlevel = 99 -- Using ufo provider need a large value, feel free to decrease the value
      vim.o.foldlevelstart = 99
      vim.o.foldenable = true
    end,
  },
  {
    "stevearc/quicker.nvim",
    opts = {},
  },
  -- {
  --   "yorickpeterse/nvim-pqf",
  --   opts = {
  --     signs = {
  --       error = { text = "✗", hl = "DiagnosticSignError" },
  --       warning = { text = "⚠", hl = "DiagnosticSignWarn" },
  --       info = { text = "🛈", hl = "DiagnosticSignInfo" },
  --       hint = { text = "", hl = "DiagnosticSignHint" },
  --     },
  --     -- By default, only the first line of a multi line message will be shown.
  --     -- When this is true, multiple lines will be shown for an entry, separated by
  --     -- a space
  --     show_multiple_lines = true,
  --
  --     -- How long filenames in the quickfix are allowed to be. 0 means no limit.
  --     -- Filenames above this limit will be truncated from the beginning with
  --     -- `filename_truncate_prefix`.
  --     max_filename_length = 0,
  --
  --     -- Prefix to use for truncated filenames.
  --     filename_truncate_prefix = "[...]",
  --   },
  --   event = "UIEnter",
  -- },
  { "MunifTanjim/nui.nvim", lazy = true },
  { "nvim-tree/nvim-web-devicons", lazy = true },
  {
    "rachartier/tiny-devicons-auto-colors.nvim",
    event = "VeryLazy",
    config = true,
  },
  {
    -- TODO: remove this later
    "stevearc/dressing.nvim",
    opts = {
      input = {
        -- Set to false to disable the vim.ui.input implementation
        enabled = false,
      },
    },
  },
  {
    "OXY2DEV/helpview.nvim",
    lazy = false, -- Recommended
    dependencies = {
      "nvim-treesitter/nvim-treesitter",
    },
  },
  {
    "brenoprata10/nvim-highlight-colors",
    event = "VeryLazy",
    opts = {
      render = "virtual",
      enable_tailwind = true,
    },
  },
  {
    "nvzone/minty",
    cmd = { "Shades", "Huefy" },
    dependencies = {
      { "nvzone/volt", lazy = true },
    },
  },
  {
    "saghen/blink.cmp",
    dependencies = { "rafamadriz/friendly-snippets", "xzbdmw/colorful-menu.nvim" },
    -- use a release tag to download pre-built binaries
    version = "*",
    -- or
    -- build = 'cargo build --release',
    opts = {
      keymap = {
        preset = "enter",
      },
      appearance = { nerd_font_variant = "mono" },
      sources = {
        default = { "lsp", "path", "snippets", "buffer" },
      },
      completion = {
        menu = {
          border = "rounded",
          draw = {
            -- We don't need label_description now because label and label_description are already
            -- conbined together in label by colorful-menu.nvim.
            columns = { { "kind_icon" }, { "label", gap = 1 } },
            components = {
              label = {
                text = function(ctx)
                  return require("colorful-menu").blink_components_text(ctx)
                end,
                highlight = function(ctx)
                  return require("colorful-menu").blink_components_highlight(ctx)
                end,
              },
            },
          },
        },
        documentation = { auto_show = true, window = { border = "rounded" } },
        list = { selection = { preselect = false, auto_insert = true } },
        ghost_text = { enabled = true },
      },
      signature = { enabled = true, window = { border = "rounded" } },
			fuzzy = { prebuilt_binaries = { download = true } },
    },
    opts_extend = { "sources.default" },
  },
}
