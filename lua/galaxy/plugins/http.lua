vim.filetype.add({
  extension = {
    ["http"] = "http",
    ["rest"] = "http",
  },
})

return {
  {
    "mistweaverco/kulala.nvim",
    opts = {
      -- default_view, body or headers
      default_view = "headers_body",
      -- -- dev, test, prod, can be anything
      -- -- see: https://learn.microsoft.com/en-us/aspnet/core/test/http-files?view=aspnetcore-8.0#environment-files
      -- default_env = "dev",
      -- -- enable/disable debug mode
      -- debug = true,
      -- -- default formatters for different content types
      -- formatters = {
      --   json = { "jq", "." },
      --   xml = { "xmllint", "--format", "-" },
      --   html = { "xmllint", "--format", "--html", "-" },
      -- },
      -- -- default icons
      -- icons = {
      --   inlay = {
      --     loading = "⏳",
      --     done = "✅ ",
      --   },
      --   lualine = "🐼",
      -- },
      -- -- additional cURL options
      -- -- e.g. { "--insecure", "-A", "Mozilla/5.0" }
      -- additional_curl_options = {},
      winbar = true,
    },
    ft = { "http", "rest" },
    keys = {
      {
        "<leader>kr",
        function()
          require("kulala").jump_prev()
        end,
        desc = "Run HTTP request",
        ft = { "http", "rest" },
      },
      {
        "<leader>k.",
        function()
          require("kulala").replay()
        end,
        desc = "Run last HTTP request",
        ft = { "http", "rest" },
      },
      {
        "<leader>ks",
        function()
          require("kulala").scratchpad()
        end,
        desc = "Open HTTP scratchpad",
        ft = { "http", "rest" },
      },
      {
        "<leader>ky",
        function()
          require("kulala").copy()
        end,
        desc = "Copy HTTP request as cURL",
        ft = { "http", "rest" },
      },
      {
        "<leader>kp",
        function()
          require("kulala").from_curl()
        end,
        desc = "Paste from curlPaste curl from clipboard as http request",
        ft = { "http", "rest" },
      },
      {
        "<leader>kq",
        function()
          require("kulala").close()
        end,
        desc = "Close http/rest file",
        ft = { "http", "rest" },
      },
      {
        "<leader>kt",
        function()
          require("kulala").toggle_view()
        end,
        desc = "Toggle Body/Headers view",
        ft = { "http", "rest" },
      },
      {
        "<leader>kf",
        function()
          require("kulala").search()
        end,
        desc = "Search for http/rest file",
      },
      {
        "[k",
        function()
          require("kulala").jump_prev()
        end,
        desc = "Prev http/rest file",
      },
      {
        "]k",
        function()
          require("kulala").jump_next()
        end,
        desc = "Next http/rest file",
      },
      {
        "<leader>kq",
        function()
          require("kulala").download_graphql_schema()
        end,
        desc = "Download GraphQL schema",
        ft = { "http", "rest" },
      },
      {
        "<leader>ke",
        function()
          require("kulala").set_selected_env()
        end,
        desc = "Select env file for HTTP requests",
        ft = { "http", "rest" },
      },
      {
        "<leader>kss",
        function()
          require("kulala").show_stats()
        end,
        desc = "Show statistics for last run request",
        ft = { "http", "rest" },
      },
      {
        "<leader>ki",
        function()
          require("kulala").inspect()
        end,
        desc = "Inspect current request",
        ft = { "http", "rest" },
      },
    },
  },
}
