------------------------------------------------------------
-- LSP Config
------------------------------------------------------------

local function list_workspace_folders()
  print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
end

local function lsp_format()
  vim.lsp.buf.format({ async = true })
end

local lsp_galaxy_config = vim.api.nvim_create_augroup("UserLspConfig", {})
local lsp_user_config = vim.api.nvim_create_augroup("UserLspConfig", {})

vim.api.nvim_create_autocmd("LspAttach", {
  group = lsp_galaxy_config,
  callback = function(ev)
    -- vim.bo[ev.buf].formatexpr = "v:lua.vim.lsp.tagfunc"
    local bufopts = { buffer = ev.buf }
    local client = vim.lsp.get_client_by_id(ev.data.client_id)

    vim.keymap.set("n", "<leader>gD", vim.lsp.buf.declaration, bufopts)
    vim.keymap.set("n", "<leader>gi", vim.lsp.buf.implementation, bufopts)
    vim.keymap.set("n", "<leader>wa", vim.lsp.buf.add_workspace_folder, bufopts)
    vim.keymap.set("n", "<leader>wr", vim.lsp.buf.remove_workspace_folder, bufopts)
    vim.keymap.set("n", "<leader>wl", list_workspace_folders, bufopts)
    vim.keymap.set("n", "<leader>D", vim.lsp.buf.type_definition, bufopts)
    vim.keymap.set("n", "<leader>cf", lsp_format, bufopts)

    vim.api.nvim_create_autocmd({ "CursorMoved" }, {
      group = lsp_user_config,
      buffer = ev.buf,
      callback = function()
        vim.lsp.buf.clear_references()
      end,
    })

    vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
      group = lsp_user_config,
      buffer = ev.buf,
      callback = function()
        if client ~= nil and client.supports_method("textDocument/documentHighlight") then
          vim.lsp.buf.document_highlight()
        end

        vim.diagnostic.open_float()
      end,
    })

    if client ~= nil and client.supports_method("textDocument/codeLens") then
      vim.api.nvim_create_autocmd({ "CursorHold", "InsertLeave", "BufEnter" }, {
        group = lsp_user_config,
        buffer = ev.buf,
        callback = function()
          vim.lsp.codelens.refresh({ bufnr = ev.buf })
        end,
      })
    end

    if client ~= nil and client.supports_method("textDocument/inlayHint") then
      vim.lsp.inlay_hint.enable(true)
    end

    if client ~= nil then
      vim.lsp.completion.enable(true, client.id, ev.buf, { autotrigger = false })
    end
  end,
})

vim.diagnostic.config({
  virtual_text = true,
  -- underline = true,
  update_in_insert = true,
  severity_sort = true,
  float = {
    focusable = false,
    -- close_events = { 'InsertEnter' },
    border = "rounded",
    source = true,
    prefix = " ",
    scope = "cursor",
  },
  signs = {
    text = {
      [vim.diagnostic.severity.ERROR] = "✗",
      [vim.diagnostic.severity.WARN] = "",
      [vim.diagnostic.severity.HINT] = "",
      [vim.diagnostic.severity.INFO] = "🛈",
    },
  },
})

local mason_config = {
  -- ui = {
  --     icons = {
  --         package_installed = "✓"
  --     }
  -- }
}

local mason_lsp_config = { automatic_installation = true }

return {
  {
    "neovim/nvim-lspconfig",
    dependencies = {
      "saghen/blink.cmp",
      "mason.nvim",
      "williamboman/mason-lspconfig.nvim",
      { "DNLHC/glance.nvim", config = true },
      -- { "soulis-1256/eagle.nvim", config = true, branch = "new_features" },
      {
        "Bekaboo/dropbar.nvim",
        opts = {
          menu = {
            win_configs = { border = "rounded" },
          },
        },
      },
    },
    opts = {
      servers = {
        lua_ls = {
          settings = {
            Lua = {
              runtime = {
                -- Tell the language server which version of Lua you're using
                -- (most likely LuaJIT in the case of Neovim)
                version = "LuaJIT",
              },
              diagnostics = {
                -- Get the language server to recognize the `vim` global
                globals = { "vim" },
              },
              workspace = {
                -- Make the server aware of Neovim runtime files
                library = vim.api.nvim_get_runtime_file("", true),
              },
              codeLens = {
                enable = true,
              },
              completion = {
                callSnippet = "Replace",
              },
              -- Do not send telemetry data containing a randomized but unique identifier
              telemetry = {
                enable = false,
              },
              hint = {
                enable = true,
                setType = true,
                paramType = true,
                arrayIndex = "Enable",
                paramName = "Enable",
                semicolon = "Enable",
              },
            },
          },
        },
      },
    },
    config = function(_, opts)
      --
      -- NOTE: First needs to setup mason and mason-lspconfig before importing lspconfig
      --
      local mason = require("mason")
      local mason_lsp = require("mason-lspconfig")

      mason.setup(mason_config)
      mason_lsp.setup(mason_lsp_config)

      --
      -- LSP Config
      --

      local lspconfig = require("lspconfig")

      local capabilities = vim.lsp.protocol.make_client_capabilities()
      capabilities = vim.tbl_deep_extend("force", capabilities, require("blink.cmp").get_lsp_capabilities({}, false))

      capabilities = vim.tbl_deep_extend("force", capabilities, {
        textDocument = {
          foldingRange = {
            dynamicRegistration = false,
            lineFoldingOnly = true,
          },
        },
        experimental = {
          { snippetTextEdit = true },
        },
      })

      for server, server_opts in pairs(opts.servers) do
        local final_opts = vim.tbl_extend("force", server_opts or {}, {
          capabilities = vim.deepcopy(capabilities),
        })

        lspconfig[server].setup(final_opts)
      end

      vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float, { noremap = true, silent = true })
      vim.keymap.set("n", "<leader>q", vim.diagnostic.setloclist, { noremap = true, silent = true })
      vim.keymap.set("n", "<leader>wq", vim.diagnostic.setqflist, { noremap = true, silent = true })
      vim.keymap.set("n", "<leader>cl", vim.lsp.codelens.run, { noremap = true, silent = true })
    end,
  },
  {
    "williamboman/mason.nvim",
    cmd = "Mason",
    lazy = true,
  },
}
