return {
  {
    "rmagatti/auto-session",
    lazy = false,
    keys = {
      { "<leader>fw", "<cmd>SessionSearch<CR>", desc = "Session search" },
    },
    opts = {
      enabled = true,
      pre_save_cmds = { "Neotree close" },
      use_git_branch = true,
      cwd_change_handling = true,
      session_lens = {
        theme_conf = {
          border = true,
          -- layout_config = {
          --   width = 0.8, -- Can set width and height as percent of window
          --   height = 0.5,
          -- },
        },
      },
    },
  },
  {
    "nvim-neo-tree/neo-tree.nvim",
    branch = "v3.x",
    cmd = { "Neotree" },
    keys = {
      {
        "<leader>fn",
        function()
          require("neo-tree.command").execute({ toggle = true, position = "float" })
        end,
        mode = { "n" },
        desc = "Toggle Explorer NeoTree",
      },
    },
    opts = {
      source_selector = {
        winbar = true,
      },
      default_component_configs = {
        indent = {
          with_expanders = true, -- if nil and file nesting is enabled, will enable expanders
          expander_collapsed = "",
          expander_expanded = "",
          expander_highlight = "NeoTreeExpander",
        },
      },
    },
    init = function()
      vim.g.loaded_netrw = 1
      vim.g.loaded_netrwPlugin = 1
      vim.g.neo_tree_remove_legacy_commands = 1
    end,
  },
  {
    "windwp/nvim-autopairs",
    event = "VeryLazy",
    opts = {
      check_ts = true,
      fast_wrap = {},
    },
  },
  {
    "kylechui/nvim-surround",
    event = "VeryLazy",
    opts = {
      keymaps = {
        insert = "<C-g>ms",
        insert_line = "<C-g>mS",
        normal = "mys",
        normal_cur = "myss",
        normal_line = "myS",
        normal_cur_line = "mySS",
        visual = "mS",
        visual_line = "mgS",
        delete = "mds",
        change = "mcs",
        change_line = "mcS",
      },
    },
  },
  { "nvim-lua/plenary.nvim", lazy = true },
}
