return {
  "nvim-treesitter/nvim-treesitter",
  optional = true,
  build = function()
    vim.cmd.TSUpdate()
  end,
  config = function(_, opts)
    vim.treesitter.language.register("typescript", { "javascript", "javascriptreact", "jsx" })
    require("nvim-treesitter.configs").setup(opts)

    vim.schedule(function()
      require("lazy").load({
        plugins = {
          "hlargs.nvim",
          "nvim-treesitter-textobjects",
          "nvim-treesitter-refactor",
        },
      })
    end)
  end,
  opts = {
    -- autopairs = { enable = true },
    -- indent = { enable = true },
    highlight = {
      enable = true,
      use_languagetree = true,
    },
    -- incremental_selection = {
    --   enable = true,
    --   keymaps = {
    --     -- init_selection = "gnn",
    --     -- node_incremental = "grn",
    --     -- scope_incremental = "grc",
    --     -- node_decremental = "grm",
    --   },
    -- },
    refactor = {
      highlight_definitions = { enable = true },
      highlight_current_scope = { enable = true },
      smart_rename = {
        enable = false,
        keymaps = {
          -- smart_rename = "grr",
        },
      },
      navigation = {
        enable = false,
        keymaps = {
          goto_definition = "gnd",
          list_definitions = "gnD",
          list_definitions_toc = "gO",
          goto_next_usage = "<a-*>",
          goto_previous_usage = "<a-#>",
        },
      },
    },
    textobjects = {
      select = {
        enable = true,
        lookahead = true,
        keymaps = {
          ["af"] = "@function.outer",
          ["if"] = "@function.inner",
          ["aC"] = "@class.outer",
          ["iC"] = "@class.inner",
          ["ac"] = "@conditional.outer",
          ["ic"] = "@conditional.inner",
          ["ar"] = "@frame.outer",
          ["ir"] = "@fram..inner",
          ["ae"] = "@block.outer",
          ["ie"] = "@block.inner",
          ["al"] = "@loop.outer",
          ["il"] = "@loop.inner",
          ["is"] = "@statement.inner",
          ["as"] = "@statement.outer",
          ["ad"] = "@comment.outer",
          ["am"] = "@call.outer",
          ["im"] = "@call.inner",
          ["aa"] = "@parameter.inner",
          ["ia"] = "@parameter.outer",
        },
      },
      -- swap = {
      --   enable = true,
      --   swap_next = {
      --     ["<a-p>"] = "@parameter.inner",
      --   },
      --   swap_previous = {
      --     ["<a-P>"] = "@parameter.inner",
      --   },
      -- },
      -- move = {
      --   enable = true,
      --   set_jumps = true,
      --   goto_next_start = {
      --     ["]m"] = "@function.outer",
      --     ["]]"] = "@class.outer",
      --     ["]a"] = "@parameter.outer",
      --   },
      --   goto_next_end = {
      --     ["]M"] = "@function.outer",
      --     ["]["] = "@class.outer",
      --   },
      --   goto_previous_start = {
      --     ["[m"] = "@function.outer",
      --     ["[["] = "@class.outer",
      --     ["[a"] = "@parameter.outer",
      --   },
      --   goto_previous_end = {
      --     ["[M"] = "@function.outer",
      --     ["[]"] = "@class.outer",
      --   },
      -- },
      -- lsp_interop = {
      --   enable = true,
      --   border = "rounded",
      --   peek_definition_code = {
      --     ["<leader>df"] = "@function.outer",
      --     ["<leader>dF"] = "@class.outer",
      --   },
      -- },
    },
  },
  dependencies = {
    { "m-demare/hlargs.nvim", config = true, lazy = true },
    { "nvim-treesitter/nvim-treesitter-textobjects", lazy = true },
    { "nvim-treesitter/nvim-treesitter-refactor", lazy = true },
  },
}
